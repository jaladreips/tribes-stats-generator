import pandas as pd
import json
import requests as r
import collections as cl

import argparse

def parser():
    p = argparse.ArgumentParser()
    p.add_argument("files", nargs="+", help="List of files to generate page content")

    return p.parse_args()

def get_stats(*links):
    players_by_hash = {}
    for link in links:
        stats = json.loads(r.get(link).text)
        for player in stats["team1"] + stats["team2"]:
            ip_hash = player["ipHashFirstTwo"]
            if ip_hash in players_by_hash:
                players_by_hash[ip_hash].append(player)
            else:
                players_by_hash[ip_hash] = [player]

    return players_by_hash


def parse_player_stats(stats):
    return pd.Series({
        "Name": cl.Counter([s["name"] for s in stats]).most_common(1)[0][0],
        "Kills": sum([s["kills"] for s in stats]),
        "Captures": sum([s["flagCaptureStat"] for s in stats]),
        "Returns": sum([s["flagReturnStat"] for s in stats]),
        "Midairs": sum([s["statMA"] for s in stats]),
        "Highest speed": max([s["StatHighestSpeed"] for s in stats if s["StatHighestSpeed"] < 450]),
        "Total score": sum([s["score"] for s in stats]),
        "Total teamkills": sum([s["StatTeamKill"] for s in stats]),
        "Maps played": len(stats)
    })


def parse_one_file(filepath):
    # such a retarded function
    match_dict = json.load(open(filepath))
    raw_stats = get_stats(*match_dict["links"])
    df = pd.concat([parse_player_stats(s) for s in list(raw_stats.values())], axis=1).transpose().set_index("Name")
    del df.index.name

    html_table = df.to_html(classes=["table", "table-bordered", "playertable", "table-hover"], justify="left")
    replacements = [
        ('<th>Kills</th>', '<th class="score">Kills</th>'),
        ('<th>Captures</th>', '<th class="score">Captures</th>'),
        ('<th>Returns</th>', '<th class="score">Returns</th>'),
        ('<th>Midairs</th>', '<th class="score">Midairs</th>'),
        ('<th>Highest speed</th>', '<th class="score">Highest speed</th>'),
        ('<th>Total score</th>', '<th class="score">Total score</th>'),
        ('<th>Total teamkills</th>', '<th class="score">Total teamkills</th>'),
        ('<th>Maps played</th>', '<th class="score">Maps played</th>'),
    ]
    for rep in replacements:
        html_table = html_table.replace(*rep)

    return """
    <h4><center>{title}</center></h4>
    {table}
    """.format(title=match_dict["title"], table=html_table)


def main():
    args = parser()
    html_tables = "\n".join([parse_one_file(file) for file in args.files])
    with open("template.html") as t, open("result.html", "wb") as f:
        f.write(t.read().format(body=html_tables).encode("utf-8"))

if __name__ == "__main__":
    main()